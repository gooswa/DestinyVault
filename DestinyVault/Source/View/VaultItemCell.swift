//
//  VaultItemCell.swift
//  DestinyVault
//
//  Created by Justin Gaussoin on 3/14/15.
//  Copyright (c) 2015 Justin Gaussoin. All rights reserved.
//

import UIKit
import Haneke

class VaultItemCell: UITableViewCell {
    var item:DestinyItem?
    
    var iconImageView:UIImageView = UIImageView(frame: CGRectMake(8,8,48,48))
    var titleLabel:UILabel = UILabel(frame: CGRectMake(65, 10, 200, 18))
    var stackLabel:UILabel = UILabel(frame: CGRectMake(32, 32, 24, 24))
    
    
    var transferButton:UIButton = UIButton()
    var equipButton:UIButton = UIButton()
    
    var iconURL:String = "" {
        didSet {
            if !iconURL.isEmpty {
                let url:NSURL = NSURL(string: "\(DestinyAPI.baseURL)\(iconURL)")!
                iconImageView.hnk_setImageFromURL(url)
            }
        }
    }

    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        titleLabel.font = UIFont(name: "HelveticaNeue", size: 16.0)
        titleLabel.textColor = UIColor.whiteColor()
        
        backgroundColor = UIColor.clearColor()
        
        contentView.opaque = false
        stackLabel.font = UIFont(name: "HelveticaNeue", size: 13.0)
        stackLabel.textAlignment = .Center

        iconImageView.layer.borderColor = UIColor.whiteColor().CGColor
        iconImageView.layer.borderWidth = 2.0
        contentView.addSubview(iconImageView)
        contentView.addSubview(titleLabel)
        self.selectionStyle = UITableViewCellSelectionStyle.None
        
        transferButton.frame = CGRectMake(318, 10, 32, 32)
        transferButton.alpha = 0.5
        
        equipButton.frame = CGRectMake(281, 10, 32, 32)
        equipButton.alpha = 0.5
        
        transferButton.setImage(UIImage(named: "TransferIcon"), forState: .Normal)
        equipButton.setImage(UIImage(named: "EquipIcon"), forState: .Normal)
        
        
        contentView.addSubview(transferButton)
        contentView.addSubview(equipButton)
        
        transferButton.hidden = true
        equipButton.hidden = true
        
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        backgroundColor = UIColor.clearColor()
        stackLabel.removeFromSuperview()
        
        transferButton.hidden = true
        equipButton.hidden = true
        transferButton.removeTarget(nil, action: nil, forControlEvents: UIControlEvents.AllEvents)
        equipButton.removeTarget(nil, action: nil, forControlEvents: UIControlEvents.AllEvents)
        
    }
    
    func configureView(item:DestinyItem) {
        self.item = item
        iconURL = item.icon
        titleLabel.text = item.itemName
        
        backgroundColor = item.isEquipped ? UIColor(hex: 0x777777, 0.4) : UIColor.clearColor()
        
        
        if !item.isEquipment && item.stackSize > 1 {
            stackLabel.text = "\(item.stackSize)"
            stackLabel.backgroundColor = UIColor.whiteColor()
            contentView.addSubview(stackLabel)
        }
        
        
        if item.transferStatus == 0 {
            transferButton.hidden = false
        }
        
        if item.canEquip {
            equipButton.hidden = false
        }
        
        if item.isEquipped {
            transferButton.hidden = true
            equipButton.hidden = true
        }
    }
    
}
