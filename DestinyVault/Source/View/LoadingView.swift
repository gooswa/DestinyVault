//
//  LoadingView.swift
//  DestinyVault
//
//  Created by Justin Gaussoin on 3/14/15.
//  Copyright (c) 2015 Justin Gaussoin. All rights reserved.
//

import UIKit

class LoadingView: UIView {
    var imageView:UIImageView = UIImageView(frame: CGRectMake(0, 0, 95, 88))
    let toOpacity:CGFloat = 0.1
    
    override init(frame:CGRect) {
        super.init(frame: frame)
        //self.backgroundColor = UIColor(hex: 0x000000, 0.65)
        imageView.image = UIImage(named: "DestinyLogo")
        
        imageView.frame.origin = CGPointMake( bounds.width/2 - imageView.bounds.width/2, bounds.height/2 - imageView.bounds.height/2 )
        addSubview(imageView)
        
        play()
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func play() {
        var fade = CABasicAnimation(keyPath: "loadingFade")
        fade.keyPath = "opacity"
        fade.toValue = toOpacity //[NSNumber numberWithFloat:opacity / 100];
        fade.autoreverses = true
        fade.duration = 1.0
        fade.repeatCount = 100
        fade.removedOnCompletion = false
        fade.fillMode = kCAFillModeForwards
        fade.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        
        self.imageView.layer.addAnimation(fade, forKey:  "loadingFade")
    }
}
