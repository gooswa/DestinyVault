//
//  CharacterEquipViewController.swift
//  DestinyVault
//
//  Created by Justin Gaussoin on 3/15/15.
//  Copyright (c) 2015 Justin Gaussoin. All rights reserved.
//

import UIKit

class CharacterEquipViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, DestinyCharacterDelegate  {
    
    var equipmentView:CharacterEquipView { return self.view as! CharacterEquipView }
    var tableView:UITableView { return self.equipmentView.tableView }
    
    var character:DestinyCharacter? {
        didSet {
            self.equipment = self.character!.inventory
            equipmentView.tableView.reloadData()
            loadingView?.removeFromSuperview()

            self.character!.delegate = self
        }
    }
    
    var loadingView:LoadingView?
    
    var equipment:[DestinyBucket] = [DestinyBucket]()
    
    
    
    var transferItem:DestinyItem?
    
    override func loadView() {
        view = CharacterEquipView(frame: MainView.contentFrame)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        println("CharacterEquipViewController.viewDidLoad")
        
        
        tableView.backgroundColor = UIColor.clearColor()
        tableView.registerClass(VaultItemCell.self, forCellReuseIdentifier: "VaultItemCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = 64.0
        tableView.separatorStyle = .None
        tableView.opaque = false
        
    }
    
    
    func updatedCharacter(character:DestinyCharacter) {
        self.character = character
    }
    
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return equipment[section].bucketName
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 37
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var view = UIView(frame: CGRectMake(0, 0, 355, 37))
        
        
        var titleLabel:UILabel = UILabel(frame: CGRectMake(5, 15, 355, 18))
        
        titleLabel.font = UIFont(name: "HelveticaNeue", size: 18.0)
        titleLabel.textColor = UIColor.whiteColor()
        titleLabel.text = equipment[section].bucketName
        view.addSubview(titleLabel)
        
        let dividerLine:CAShapeLayer = CreateDividerLine(CGPointMake(0, 36.5), CGPointMake(365, 36.5), width: 1.0)
        view.layer.addSublayer(dividerLine)
        
        
        return view
    }
 
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return equipment.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return equipment[section].items.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell:VaultItemCell = tableView.dequeueReusableCellWithIdentifier("VaultItemCell") as! VaultItemCell
        

        var items:[DestinyItem] = equipment[indexPath.section].items
        let item:DestinyItem = items[indexPath.row]
        cell.configureView(item)
        
        if item.transferStatus == 0 {
            cell.transferButton.addTarget(self, action: Selector("transferItemHandler:"), forControlEvents: .TouchUpInside)
        }
        
        if item.canEquip && item.isEquipment && !item.isEquipped {
            cell.equipButton.addTarget(self, action: Selector("equipItemHandler:"), forControlEvents: .TouchUpInside)
        }

        return cell
    }
    
    func transferItemHandler(button:UIButton) {
        
        if let itemCell = button.superview?.superview as? VaultItemCell {
            let item = itemCell.item
            transferItem = item
            let noteCenter = NSNotificationCenter.defaultCenter()
            let notification:NSNotification = NSNotification(name: "TranferItemTargetOpen", object: nil)
            noteCenter.postNotification(notification)
            
            noteCenter.addObserver(self, selector: Selector("transferItemTargetSelected:"), name: "TranferItemTargetSelected", object: nil)
        }
        
    }
    
    
    func equipItemHandler(button:UIButton) {

        if let itemCell = button.superview?.superview as? VaultItemCell, item = itemCell.item, let char = character {
            var model:DestinyModel = DestinyModel.sharedInstance
            self.insertLoadingView()
            model.equipItem(item, character: char) {results, error in
                model.characters[char.characterId]?.update {}
            }
            
        }
        
    }
    
    func insertLoadingView() {
        loadingView = LoadingView(frame: self.view.bounds)
        loadingView!.play()
        self.view.addSubview(loadingView!)
    }
    
    func transferItemTargetSelected(note:NSNotification) {
        let noteCenter = NSNotificationCenter.defaultCenter()
        noteCenter.removeObserver(self, name: "TranferItemTargetSelected", object: nil)
        if let characterId = note.object as? String {
            
            var model:DestinyModel = DestinyModel.sharedInstance
            
            if let item = transferItem, let char = character {
                self.insertLoadingView()
                
                model.vaultTransfer(item, character: char, toVault: true) {result, error in
                    if !characterId.isEmpty {
                        if let toChar = model.characters[characterId] {
                            
                            DelayClosure(1.05, {
                                model.vaultTransfer(item, character: toChar, toVault: false) {result, error in
                                    print("vault B result \(result), error \(error)")
                                    model.characters[char.characterId]?.update {}
                                }
                            })
                            
                            
                        }
                    } else {
                        model.characters[char.characterId]?.update {}
                    }
                    
                    
                }
            }
            
            
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        //println("You selected cell #\(indexPath.row)!")
    }
}
