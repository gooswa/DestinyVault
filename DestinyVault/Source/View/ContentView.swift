//
//  ContentView.swift
//  DestinyVault
//
//  Created by Justin Gaussoin on 3/24/15.
//  Copyright (c) 2015 Justin Gaussoin. All rights reserved.
//

import UIKit


class ContentBlurLayer : CALayer {
    var blurRadius:CGFloat = 0.0
    override class func needsDisplayForKey(key: String!) -> Bool{
        if key == "blurRadius" {
            return true
        }
        return super.needsDisplayForKey(key)
    }
    
}


class ContentView: UIView {
    
    override class func layerClass() -> AnyClass {
        return ContentBlurLayer.self
    }
    
}
