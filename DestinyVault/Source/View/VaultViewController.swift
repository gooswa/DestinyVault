//
//  VaultViewController.swift
//  DestinyVault
//
//  Created by Justin Gaussoin on 3/14/15.
//  Copyright (c) 2015 Justin Gaussoin. All rights reserved.
//

import UIKit

class VaultViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, DestinyVaultDelegate {
    var vaultView:VaultView { return self.view as! VaultView }
    var tableView:UITableView { return self.vaultView.tableView }
    
    
    var buckets:[DestinyBucket] = [DestinyBucket]()
    
    var transferItem:DestinyItem?
    var vault:DestinyVault? {
        didSet {
            buckets.removeAll(keepCapacity: false)
            buckets.append(vault!.armor!)
            buckets.append(vault!.weapons!)
            buckets.append(vault!.general!)
            vault?.delegate = self
            tableView.reloadData()
        }
    }
    
    var loadingView:LoadingView?
    
    override func loadView() {
        view = VaultView(frame: MainView.contentFrame)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.backgroundColor = UIColor.clearColor()
        tableView.registerClass(VaultItemCell.self, forCellReuseIdentifier: "VaultItemCell")
        tableView.delegate      =   self
        tableView.dataSource    =   self
        tableView.rowHeight = 64.0
        tableView.separatorStyle = .None
        tableView.opaque = false
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.vault?.delegate = nil
    }
    
    
    func updatedVault(aVault:DestinyVault) {
        self.vault = aVault
        loadingView?.removeFromSuperview()
    }
    
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 37
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        var view = UIView(frame: CGRectMake(0, 0, 355, 37))

        var titleLabel:UILabel = UILabel(frame: CGRectMake(5, 15, 355, 18))
        
        titleLabel.font = UIFont(name: "HelveticaNeue", size: 18.0)
        titleLabel.textColor = UIColor.whiteColor()
        titleLabel.text = buckets[section].bucketName
        view.addSubview(titleLabel)
        
        let dividerLine:CAShapeLayer = CreateDividerLine(CGPointMake(5, 36.5), CGPointMake(365, 36.5), width: 1.0)
        view.layer.addSublayer(dividerLine)
        
        
        return view
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return buckets[section].bucketName
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return buckets.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return buckets[section].items.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell:VaultItemCell = tableView.dequeueReusableCellWithIdentifier("VaultItemCell") as! VaultItemCell

        var items:[DestinyItem] = buckets[indexPath.section].items
        let item:DestinyItem = items[indexPath.row]
        
        
        cell.configureView(item)
        

        if item.transferStatus == 0 {
            cell.transferButton.addTarget(self, action: Selector("transferItemHandler:"), forControlEvents: .TouchUpInside)
        }
        
        
        return cell
        
    }
    
    func transferItemHandler(button:UIButton) {
        
        if let itemCell = button.superview?.superview as? VaultItemCell {
            let item = itemCell.item
            transferItem = item
            let noteCenter = NSNotificationCenter.defaultCenter()
            let notification:NSNotification = NSNotification(name: "TranferItemTargetOpen", object: nil)
            noteCenter.postNotification(notification)
            
            noteCenter.addObserver(self, selector: Selector("transferItemTargetSelected:"), name: "TranferItemTargetSelected", object: nil)
        }
        
    }
    
    
    func insertLoadingView() {
        loadingView = LoadingView(frame: self.view.bounds)
        loadingView!.play()
        self.view.addSubview(loadingView!)
    }
    
    func transferItemTargetSelected(note:NSNotification) {
        let noteCenter = NSNotificationCenter.defaultCenter()
        noteCenter.removeObserver(self, name: "TranferItemTargetSelected", object: nil)
        if let characterId = note.object as? String {
            
            var model:DestinyModel = DestinyModel.sharedInstance
            
            if let item = transferItem, let char = model.characters[characterId] {
                self.insertLoadingView()
                model.vaultTransfer(item, character: char, toVault: false) {result, error in
                    model.vault.update {}
                }
            }
            
            
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        //println("You selected cell #\(indexPath.row)!")
    }
}
