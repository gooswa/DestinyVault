//
//  HeaderView.swift
//  DestinyVault
//
//  Created by Justin Gaussoin on 3/14/15.
//  Copyright (c) 2015 Justin Gaussoin. All rights reserved.
//

import UIKit

public enum MenuItemType {
    case Character
    case Vault
}


class MenuItemView: UIControl {
    
    var menuType:MenuItemType = MenuItemType.Character
    var characterIndex:Int = -1
    var characterId:String = ""
    
    var backgroundImageView:UIImageView = UIImageView(frame: CGRectMake(0, 0, 375, 75))
    var emblemImageView:UIImageView = UIImageView(frame: CGRectMake(0, 0, 60, 60))
    
    var classLabel:UILabel = UILabel(frame: CGRectMake(85, 10, 200, 24))
    var levelLabel:UILabel = UILabel(frame: CGRectMake(375-52, 10, 42, 36))
    
    convenience init() {
        self.init(frame: CGRectMake(0, 0, 375, 75))
        
        classLabel.font = UIFont(name: "HelveticaNeue", size: 22.0)
        classLabel.textColor = UIColor.whiteColor()
        
        levelLabel.font = UIFont(name: "HelveticaNeue", size: 36.0)
        levelLabel.textColor = UIColor.whiteColor()
        
        backgroundColor = UIColor.blackColor()
        addSubview(backgroundImageView)
        addSubview(emblemImageView)
        addSubview(classLabel)
        addSubview(levelLabel)
    }
}
