//
//  CharacterEquipView.swift
//  DestinyVault
//
//  Created by Justin Gaussoin on 3/15/15.
//  Copyright (c) 2015 Justin Gaussoin. All rights reserved.
//

import UIKit

class CharacterEquipView: UIView {

    var tableView:UITableView = UITableView(frame: CGRectMake(10, 0, 355, 667-75), style: .Grouped)
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        tableView.showsVerticalScrollIndicator = false
        addSubview(tableView)
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
