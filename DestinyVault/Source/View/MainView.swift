//
//  MainView.swift
//  DestinyVault
//
//  Created by Justin Gaussoin on 3/14/15.
//  Copyright (c) 2015 Justin Gaussoin. All rights reserved.
//

import UIKit



class MainView: UIView {
    
    static var openPosition:CGFloat = 367.0
    static var openTransferPosition:CGFloat = 442.0
    static var closedPosition:CGFloat = 592.0
    
    static var contentFrame:CGRect = CGRectMake(0, 0, 375, 667-75)
    var sliderView:UIView = UIView(frame: CGRectMake(0, 667-75, 375, 300))
    
    var contentView:UIView = UIView(frame: MainView.contentFrame)

    
    var menuItems:[MenuItemView] = [MenuItemView]()
    
    var panGesutre:UIPanGestureRecognizer!
    
    var animator:UIDynamicAnimator!
    var attachBehavior:UIAttachmentBehavior?

    
    var sliderOpen:Bool = false
    
    var effectView:UIVisualEffectView?
    
    var contentViewImage:UIImage?
    var contentViewImageView:UIImageView

    required init(coder aDecoder: NSCoder) {
        
        contentViewImageView = UIImageView(frame: MainView.contentFrame)
        
        super.init(coder: aDecoder)

        //UIColor *background = [[UIColor alloc] initWithPatternImage:[UIImage imageNamed:@"Rough_White_Paper_with_tape.png"]];
        
        let color = UIColor(patternImage: UIImage(named: "MainViewBackground")!)
        backgroundColor = color
        
        addSubview(contentView)
        

        addSubview(sliderView)

        animator = UIDynamicAnimator(referenceView: self)

        panGesutre = UIPanGestureRecognizer(target: self, action: Selector("panGestureHandler:"))
        sliderView.addGestureRecognizer(panGesutre!)
        
        var effect = UIBlurEffect(style: UIBlurEffectStyle.Dark)
        effectView = UIVisualEffectView(effect: effect)
        
        effectView?.frame = contentView.frame
        effectView?.alpha = 0
        insertSubview(effectView!, belowSubview: sliderView)
        
        //contentViewImage = takeContentSnapshot()
        //contentViewImageView.image = contentViewImage
        
        //insertSubview(contentViewImageView, belowSubview: sliderView)
        
    }
    
    
    func closeMenu() {
        UIView.animateWithDuration(0.25, delay: 0, options: .BeginFromCurrentState | .CurveEaseOut, animations: {
            self.sliderView.setY(y: MainView.closedPosition)
            self.effectView?.alpha = 0
            }, completion: {finished in
                self.sliderOpen = false
        })
    }
    
    func openMenu() {
        UIView.animateWithDuration(0.25, delay: 0, options: .BeginFromCurrentState | .CurveEaseOut, animations: {
            self.sliderView.setY(y: MainView.openPosition)
            self.effectView?.alpha = 1
            }, completion: {finished in
                self.sliderOpen = true
        })
    }
    
    
    func slideItemsUp() {
        for item in menuItems {
            
            item.setY(y: item.frame.origin.y-75)
            
        }
    }
    
    func slideItemsDown() {
        for item in menuItems {
            
            item.setY(y: item.frame.origin.y+75)
            
        }
        
    }
    /*
    - (UIImage *)takeSnapshotOfView:(UIView *)view
    {
    UIGraphicsBeginImageContext(CGSizeMake(view.frame.size.width, view.frame.size.height));
    [view drawViewHierarchyInRect:CGRectMake(0, 0, view.frame.size.width, view.frame.size.height) afterScreenUpdates:YES];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
    }*/
    
    func takeContentSnapshot() -> UIImage {
        UIGraphicsBeginImageContext(contentView.frame.size)
        contentView.drawViewHierarchyInRect(contentView.bounds, afterScreenUpdates: true)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        return img
    }
    
    func panGestureHandler(gesture:UIPanGestureRecognizer) {
        
        
        
        var location:CGPoint = gesture.translationInView(self)
        location.x = sliderView.center.x
        
        if sliderView.frame.origin.y <= MainView.openPosition {
            location.y = 0
        }

        switch gesture.state {
        case .Began:

            
            attachBehavior = UIAttachmentBehavior(item: sliderView, attachedToAnchor: location)
            animator.addBehavior(attachBehavior!)
            
            
            
            
            //
            
        case .Changed:

            
            if sliderView.frame.origin.y > 367.0 {
                attachBehavior?.anchorPoint = location
            }
            
            
            
            
            let spanDiff:CGFloat = 592.0 - 367.0
            let viewPosOffset:CGFloat = sliderView.frame.origin.y - 367.0
            let ratio = 1.0 - (viewPosOffset / spanDiff)
            
            //contentView.alpha = 1.0 - ratio
            contentViewImageView.alpha = ratio
            
            //contentView.setY(y: (sliderView.frame.origin.y-592) )
            
        case .Ended:
            if let attach = attachBehavior {
                animator.removeBehavior(attach)
                attachBehavior = nil
            }
            
            let velocity = gesture.velocityInView(self)
            var position:CGFloat = 367.0
            var difference = sliderView.frame.origin.y - 367
            if !sliderOpen && difference > 180 {
                position = 592.0
            } else if sliderOpen {
                position = 592.0
            }
            

            UIView.animateWithDuration(0.20, delay: 0, options: .BeginFromCurrentState | .CurveEaseOut, animations: {
                self.sliderView.setY(y: position)
                
                if position == 592 {
                    //self.contentView.alpha = 1.0
                    self.effectView?.alpha = 0
                } else {
                    //self.contentView.alpha = 0
                    self.effectView?.alpha = 1.0
                }
                
                //self.contentView.setY(y: (position-592) )
                }, completion: {finished in
                    self.sliderOpen = !self.sliderOpen
                    
            })
            
        default:
            ()
        }
    }
}
