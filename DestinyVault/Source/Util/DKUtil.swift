//
//  DKUtil.swift
//  DestinyVault
//
//  Created by Justin Gaussoin on 3/14/15.
//  Copyright (c) 2015 Justin Gaussoin. All rights reserved.
//

import UIKit

func CreateDividerLine(start:CGPoint, end:CGPoint, width:CGFloat=1.0, color:UIColor=UIColor.whiteColor()) -> CAShapeLayer {
    var layer = CAShapeLayer()
    var path = CGPathCreateMutable()
    
    CGPathMoveToPoint(path, nil, start.x, start.y)
    CGPathAddLineToPoint(path, nil, end.x, end.y)
    layer.lineWidth = width
    layer.strokeColor = color.CGColor
    layer.path = path
    
    return layer
}

func DelayClosure(delay:Float, closure:()->()) {
    dispatch_after(
        dispatch_time(
            DISPATCH_TIME_NOW,
            Int64(delay * Float(NSEC_PER_SEC))
        ), dispatch_get_main_queue(), closure)
}

extension UIColor {
    convenience init(hex hexValue:UInt32, _ alpha:CGFloat=1.0) {
        let redValue = CGFloat((hexValue & 0xFF0000) >> 16)/256.0
        let greenValue = CGFloat((hexValue & 0xFF00) >> 8)/256.0
        let blueValue = CGFloat(hexValue & 0xFF)/256.0
        self.init(red: redValue, green: greenValue, blue: blueValue, alpha: alpha)
    }
}


extension UIView {
    func setY(#y:CGFloat) {
        var frame:CGRect = self.frame
        frame.origin.y = y
        self.frame = frame
    }
}