//
//  DestinyVault.swift
//  DestinyVault
//
//  Created by Justin Gaussoin on 3/14/15.
//  Copyright (c) 2015 Justin Gaussoin. All rights reserved.
//

import Foundation
import Argo
import Runes


public protocol DestinyVaultDelegate {
    func updatedVault(vault:DestinyVault)
}

public class DestinyVault {
    var armor:DestinyBucket?
    var weapons:DestinyBucket?
    var general:DestinyBucket?
    
    

    
    var delegate:DestinyVaultDelegate?
    
    var needsUpdating:Bool = false {
        willSet(newValue) {
            if needsUpdating && newValue { return }
            update { self.needsUpdating = false }
        }
    }
    
    func update(callback:()->Void) {

            var model:DestinyModel = DestinyModel.sharedInstance
            model.client.getVault(model.user)
                {(buckets) in


                    var queue = dispatch_queue_create("com.destiny.update.vault", DISPATCH_QUEUE_CONCURRENT)
                    dispatch_async(queue) {
                        var buckets = JSONValue.parse <^> buckets
                        for bucket in buckets {
                            let destBucket = DestinyBucket.decode(bucket)
                            switch destBucket!.bucketName {
                            case "General":
                                self.general = destBucket
                            case "Armor":
                                self.armor = destBucket
                            case "Weapons":
                                self.weapons = destBucket
                            default:
                                println("Unknown Bucket")
                            }
                        }
                        dispatch_async(dispatch_get_main_queue()) {
                            self.delegate?.updatedVault(self)
                            callback()
                        }
                    }
                    
            }

        
    }
}
