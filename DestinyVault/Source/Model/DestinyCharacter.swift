//
//  DestinyCharacter.swift
//  DestinyVault
//
//  Created by Justin Gaussoin on 3/14/15.
//  Copyright (c) 2015 Justin Gaussoin. All rights reserved.
//

import Foundation
import Argo
import Runes

public enum DestinyClass:Int {
    case Warlock = 2
    case Hunter = 1
    case Titan = 0
    
    var name:String {
        switch self {
        case .Warlock:
            return "Warlock"
        case .Hunter:
            return "Hunter"
        case .Titan:
            return "Titan"
        }
    }
}

extension DestinyClass : JSONDecodable {
    public static func decode(j: JSONValue) -> DestinyClass? {
        switch j {
        case let .JSONNumber(j): return DestinyClass(rawValue: j.integerValue)
        default: return .None
        }
    }
}




//MARK: DestinyCharacter

public protocol DestinyCharacterDelegate {
    func updatedCharacter(character:DestinyCharacter)
}

public class DestinyCharacter {
    var characterId:String
    var classType:DestinyClass
    var level:Int
    var className:String {
        return classType.name
    }
    var emblemURL:String
    var backgroundURL:String
    
    
    var delegate:DestinyCharacterDelegate?
    
    var inventory:[DestinyBucket] = [DestinyBucket]()

    init(characterId:String, classType:DestinyClass, level:Int, emblemURL:String, backgroundURL:String) {
        self.characterId = characterId
        self.classType = classType
        self.level = level
        self.emblemURL = emblemURL
        self.backgroundURL = backgroundURL
    }
    
    var needsUpdating:Bool = false {
        willSet(newValue) {
            if needsUpdating && newValue { return }
            update { self.needsUpdating = false }
        }
    }
    
    func update(callback:()->Void) {
        //println("updateCharacter")
            self.inventory.removeAll(keepCapacity: false)
        
            var model:DestinyModel = DestinyModel.sharedInstance
            model.client.getCharacterInventory(model.user, character: self)
                {(inventory) in
                    //println("parseOutCharacterInventory")
                    
                    
                    
                    var queue = dispatch_queue_create("com.destiny.update.character", DISPATCH_QUEUE_CONCURRENT)
                    dispatch_async(queue) {
                        var index = 0
                        let json = JSONValue.parse <^> inventory
                        if let equipabble: [AnyObject] = inventory["Equippable"] as? [AnyObject] {
                            
                            var buckets = JSONValue.parse <^> equipabble
                            
                            for bucket in buckets {
                                
                                if let destBucket = DestinyBucket.decode(bucket) where index < 9 {
                                    self.inventory.append(destBucket)
                                }
                                index++
                            }
                            
                            //println("character finished Equippable")
                            
                        }
                        if let item: [AnyObject] = inventory["Item"] as? [AnyObject] {
                            
                            index = 0
                            var buckets = JSONValue.parse <^> item
                            for bucket in buckets {
                                if let destBucket = DestinyBucket.decode(bucket) where index < 2 {
                                    self.inventory.append(destBucket)
                                }
                                index++
                            }
                            
                            //println("character finished Items")
                        }
                        dispatch_async(dispatch_get_main_queue()) {
                            self.delegate?.updatedCharacter(self)
                            callback()
                        }
                    }
                        
            }
        
        
    }
}


extension DestinyCharacter : JSONDecodable, DebugPrintable {
    
    public var debugDescription: String {
        get {
            return "\nDestinyCharacter(\n\tcharacterId: \(characterId),\n\tclassType: \(classType.name), \n\tlevel: \(level), \n\temblemURL: \(emblemURL)\n)"
        }
    }
    
    static func create(id: String)
        (classType: DestinyClass)
        (level: Int)
        (emblemURL: String)
        (backgroundURL: String) -> DestinyCharacter {
            
            return DestinyCharacter(
                characterId: id,
                classType: classType,
                level: level,
                emblemURL: emblemURL,
                backgroundURL: backgroundURL
            )
    }
    
    public static func decode(j: JSONValue) -> DestinyCharacter? {
        
        return DestinyCharacter.create
            <^> j <| ["characterBase", "characterId"]
            <*> j <| ["characterBase", "classType"]
            <*> j <| "characterLevel"
            <*> j <| "emblemPath"
            <*> j <| "backgroundPath"
    }
}
