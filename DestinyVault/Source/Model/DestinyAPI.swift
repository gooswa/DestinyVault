//
//  DestinyAPI.swift
//  DestinyVault
//
//  Created by Justin Gaussoin on 3/13/15.
//  Copyright (c) 2015 Justin Gaussoin. All rights reserved.
//

import Foundation
import Alamofire

public enum DestinyAPI : URLRequestConvertible {
    static let baseURL = "https://www.bungie.net"
    static let platformURL = "\(baseURL)/Platform"
    static let destinyURL = "\(platformURL)/Destiny"
    
    static let cookieNames:[String] = ["bungled", "bungleatk", "bungleme", "bungledid"]
    
    static let bungledCookie = "bungled"

    
    static var cookies:[NSObject:AnyObject]?
    static var csrf:String?
    static var key: String?
    
    case BaseURL
    case Login(DestinyPlatform)
    case CurrentUser
    case MembershipIds
    case Manifest
    case Account(DestinyUser)
    case Vault(DestinyUser)
    case CharacterInventory(DestinyUser, DestinyCharacter)
    case TransferItem(DestinyTransferObject)
    case EquipItem(DestinyEquipObject)
    
    var path:String {
        switch self {
        case .BaseURL:
            return DestinyAPI.baseURL
        case .Login(let platform):
            return "\(DestinyAPI.baseURL)/en/User/SignIn/\(platform.login)/"
        case .CurrentUser:
            return "\(DestinyAPI.platformURL)/User/GetBungieNetUser/"
        case .MembershipIds:
            return "\(DestinyAPI.platformURL)/User/GetMembershipIds/"
        case .Manifest:
            return "\(DestinyAPI.destinyURL)/Manifest/"
        case .Account(let user):
            return "\(DestinyAPI.destinyURL)/Tiger\(user.membershipType.platform)/Account/\(user.membershipId)/"
        case .Vault(let user):
            return "\(DestinyAPI.destinyURL)/\(user.membershipType.rawValue)/MyAccount/Vault/"
        case .CharacterInventory(let user, let character):
            return "\(DestinyAPI.destinyURL)/\(user.membershipType.rawValue)/Account/\(user.membershipId)/Character/\(character.characterId)/Inventory/"
        case .TransferItem(let dto):
            return "\(DestinyAPI.destinyURL)/TransferItem/"
        case .EquipItem(let deo):
            return "\(DestinyAPI.destinyURL)/EquipItem/"
        }
    }
    
    var method:Alamofire.Method {
        switch self {
        case .TransferItem(let dto):
            return .POST
        case .EquipItem(let dto):
            return .POST
        default:
            return .GET
        }
    }

    var encoding:Alamofire.ParameterEncoding {
        switch self {
        case .TransferItem(let dto):
            return .JSON
        case .EquipItem(let dto):
            return .JSON
        default:
            return .URL
        }
    }
    
    var parameters:[String:AnyObject] {
        switch self {
        case .TransferItem(let dto):
            return dto.httpDictionary
        case .EquipItem(let deo):
            return deo.httpDictionary
        default:
            return ["definitions" : "false"]
        }
    }
    
    public var URLRequest:NSURLRequest {
        let url = NSURL(string: self.path)
        
        let mutableURLRequest = NSMutableURLRequest(URL: url!)
        mutableURLRequest.HTTPMethod = method.rawValue
        
        if let cke = DestinyAPI.cookies,
            let k = DestinyAPI.key,
            let cf = DestinyAPI.csrf {
                
            mutableURLRequest.allHTTPHeaderFields = cke
            mutableURLRequest.setValue(k, forHTTPHeaderField: "X-API-Key")
            mutableURLRequest.setValue(cf, forHTTPHeaderField: "X-CSRF")
        }
        
        return encoding.encode(mutableURLRequest, parameters: parameters).0
        
    }
}
