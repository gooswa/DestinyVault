//
//  DestinyUser.swift
//  DestinyVault
//
//  Created by Justin Gaussoin on 3/13/15.
//  Copyright (c) 2015 Justin Gaussoin. All rights reserved.
//

import Foundation

public struct DestinyUser : DebugPrintable {
    let gamerTag:String
    let membershipType:DestinyPlatform
    let membershipId:String
    
    public var debugDescription: String {
        get {
            var platform:String = membershipType.platform
            return "DestinyUser(\n\tGamerTag: \(gamerTag),\n\tmembershipType: \(platform),\n\tmembershipId: \(self.membershipId)"
        }
    }
    
    init(tag:String, type:DestinyPlatform, id:String) {
        gamerTag = tag
        membershipType = type
        membershipId = id
    }
}