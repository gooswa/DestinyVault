//
//  DestinyItem.swift
//  DestinyVault
//
//  Created by Justin Gaussoin on 3/14/15.
//  Copyright (c) 2015 Justin Gaussoin. All rights reserved.
//

import Foundation
import Argo
import Runes


public struct DestinyItem {
    let itemName:String
    let itemHash:Int
    let instanceId:String
    let itemLevel:Int
    let isEquipped:Bool
    let bindStatus:Int
    let stackSize:Int
    let canEquip:Bool
    let cannotEquipReason:Int
    let isEquipment:Bool
    let isGridComplete:Bool
    let transferStatus:Int
    let icon:String
    let secondaryIcon:String
}



extension DestinyItem : JSONDecodable, DebugPrintable {
    static func create
        (itemName:String)
        (itemHash:Int)
        (instanceId:String)
        (itemLevel:Int)
        (isEquipped:Bool)
        (bindStatus:Int)
        (stackSize:Int)
        (canEquip:Bool)
        (cannotEquipReason:Int)
        (isEquipment:Bool)
        (isGridComplete:Bool)
        (transferStatus:Int)
        (icon:String)
        (secondaryIcon:String) -> DestinyItem {
            
            return DestinyItem(itemName: itemName,
                itemHash: itemHash,
                instanceId: instanceId,
                itemLevel: itemLevel,
                isEquipped: isEquipped,
                bindStatus: bindStatus,
                stackSize: stackSize,
                canEquip: canEquip,
                cannotEquipReason: cannotEquipReason,
                isEquipment: isEquipment,
                isGridComplete: isGridComplete,
                transferStatus: transferStatus,
                icon: icon,
                secondaryIcon: secondaryIcon)
    }
    
    public var debugDescription: String {
        get {
            let itemMirror = reflect(self)
            var debugString = "\nDestinyItem(\n"
            for var i = 0; i < itemMirror.count; i++ {
                let (propertyName, childMirror) = itemMirror[i]
                debugString += "\t\(propertyName): \(childMirror.value),\n"

            }
            debugString += ")\n"
            return debugString
        }
    }
    
    public static func decode(j:JSONValue) -> DestinyItem? {
        let bucketHashInt:Int = (j <| "itemHash")!
        var manifest = DestinyModel.sharedInstance.manifest
        var itemInfo = manifest.findDefinition(DestinyManifest.DestinyItemTable, hash: bucketHashInt)
        println("\n\n bucketHashInt: \(bucketHashInt) \n \(itemInfo) \n\n")
        var name:String, icon:String, secondaryIcon:String
        if let info = itemInfo {
            name = (itemInfo?["itemName"] as! String)
            icon = (itemInfo?["icon"] as! String)
            secondaryIcon = (itemInfo?["secondaryIcon"] as! String)
        } else {
            name = "Unknown"
            icon = "/img/misc/missing_icon.png"
            secondaryIcon = "/img/misc/missing_icon.png"
        }
        
        return DestinyItem.create
            <^> name
            <*> j <| "itemHash"
            <*> j <| "itemInstanceId"
            <*> j <| "itemLevel"
            <*> j <| "isEquipped"
            <*> j <| "bindStatus"
            <*> j <| "stackSize"
            <*> j <| "canEquip"
            <*> j <| "cannotEquipReason"
            <*> j <| "isEquipment"
            <*> j <| "isGridComplete"
            <*> j <| "transferStatus"
            <*> icon
            <*> secondaryIcon
    }
}


