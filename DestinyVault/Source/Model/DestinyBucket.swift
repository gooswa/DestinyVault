//
//  DestinyBucket.swift
//  DestinyVault
//
//  Created by Justin Gaussoin on 3/14/15.
//  Copyright (c) 2015 Justin Gaussoin. All rights reserved.
//

import Foundation
import Argo
import Runes



public struct DestinyBucket {
    let bucketName:String
    let bucketHash:Int
    let bucketDescription:String
    
    var items:[DestinyItem]
    
    init(name:String, hash:Int, description:String, items:[DestinyItem]) {
        bucketName = name
        bucketHash = hash
        bucketDescription = description
        self.items = items
        self.items.sort { return $0.0.isEquipped }
    }
}

extension DestinyBucket: JSONDecodable, DebugPrintable {
    
    public var debugDescription: String {
        get {
            return "\nDestinyBucket(\n\tbucketName: \(bucketName),\n\tbucketHash: \(bucketHash),\n\tbucketDescription: \(bucketDescription),\n\t items: \(items)"
        }
    }
    
    static func create(name: String)
        (hash: Int)
        (description: String)
        (items:[DestinyItem])-> DestinyBucket {
            return DestinyBucket(name: name, hash: hash, description: description, items: items)
    }
    
    public static func decode(j: JSONValue) -> DestinyBucket? {
        let bucketHash:Int = (j <| "bucketHash")!
        var manifest = DestinyModel.sharedInstance.manifest
        var bucketInfo = manifest.findDefinition(DestinyManifest.DestinyBucketTable, hash: bucketHash)
        
        return DestinyBucket.create
            <^> (bucketInfo?["bucketName"] as! String)
            <*> bucketHash
            <*> (bucketInfo?["bucketDescription"] as! String)
            <*> j <|| "items"
    }
}
