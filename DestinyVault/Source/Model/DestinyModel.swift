//
//  DestinyModel.swift
//  DestinyVault
//
//  Created by Justin Gaussoin on 3/13/15.
//  Copyright (c) 2015 Justin Gaussoin. All rights reserved.
//

import Foundation
import Argo
import Runes

class DestinyModel {
    
    private var _loadedCallback:((DestinyUser, NSError?) -> Void)?
    var client:DestinyClient
    var user:DestinyUser!
    var characters:[String:DestinyCharacter]
    var vault:DestinyVault
    var manifest:DestinyManifest
    
    
    var transferQueue:[DestinyAPI] = [DestinyAPI]()

    
    private var _accountData:[String:AnyObject]?
    
    class var sharedInstance: DestinyModel {
        struct Static {
            static var onceToken: dispatch_once_t = 0
            static var instance: DestinyModel? = nil
        }
        dispatch_once(&Static.onceToken) {
            Static.instance = DestinyModel()
        }
        return Static.instance!
    }
    
    private init() {
        client = DestinyClient(apiKey: "e99d212235144bfca899d3b1fa310d14")
        characters = [String:DestinyCharacter]()
        manifest = DestinyManifest()
        vault = DestinyVault()
    }
    
    
    func login(callback:(DestinyUser, NSError?)->Void) {
        //println("login")
        _loadedCallback = callback
        
        client.authenticate("social@dubink.com", password: "D!r26psn")
        {(user, error) in
            
            if let err = error {
                println(error)
                return
            }
            
            self.user = user
            
            self.manifest.checkManifestUpdate {
                self.getCharacters()
            }
        }
        
    }

    
    func equipItem(item:DestinyItem, character:DestinyCharacter, _ callback:((AnyObject?, NSError?) -> Void) ) {
        var equipObject = DestinyEquipObject(
            characterId: character.characterId,
            membershipType: user.membershipType,
            itemId: item.instanceId
        )
    
        var endpoint:DestinyAPI = DestinyAPI.EquipItem(equipObject)
        client.apiRequest(endpoint, callback)
    }
    
    func vaultTransfer(item:DestinyItem, character:DestinyCharacter, toVault:Bool = false, _ callback:((AnyObject?, NSError?) -> Void) ) {
        var transferObject = DestinyTransferObject(
            characterId: character.characterId,
            membershipType: user.membershipType,
            itemId: item.instanceId,
            itemReferenceHash: item.itemHash,
            stackSize: item.stackSize,
            transferToVault: toVault
        )
        
        var endpoint:DestinyAPI = DestinyAPI.TransferItem(transferObject)
        
        client.apiRequest(endpoint, callback)
    }
    


    func getCharacters() {
        client.getAccount(user)
        {[unowned self] (account) in
            if let acc = account as? [String:AnyObject],
                let characters = acc["characters"] as? [AnyObject] {
                    
                for character in characters {
                    let json = JSONValue.parse <^> character
                    let destinyCharacter = DestinyCharacter.decode(json!)
                    self.characters[destinyCharacter!.characterId] = destinyCharacter
  
                    
                }
                self._loadedCallback?(self.user, nil)
            }
                
        }
    }
    
    
    
    
    
    
}