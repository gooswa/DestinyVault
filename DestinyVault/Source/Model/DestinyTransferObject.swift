//
//  DestinyTransferObject.swift
//  DestinyVault
//
//  Created by Justin Gaussoin on 3/15/15.
//  Copyright (c) 2015 Justin Gaussoin. All rights reserved.
//

import Foundation

public struct DestinyTransferObject {
    var characterId:String
    var membershipType:DestinyPlatform
    var itemId:String
    var itemReferenceHash:Int
    var stackSize:Int
    var transferToVault:Bool
    
    var httpDictionary:[String:AnyObject] {
        
        var dict = [String:AnyObject]()
        dict["characterId"] = characterId
        dict["membershipType"] = membershipType.rawValue
        dict["itemId"] = itemId
        dict["itemReferenceHash"] = itemReferenceHash
        dict["stackSize"] = stackSize
        dict["transferToVault"] = transferToVault
        return dict
    }
}

public struct DestinyEquipObject {
    var characterId:String
    var membershipType:DestinyPlatform
    var itemId:String
    
    var httpDictionary:[String:AnyObject] {
        var dict = [String:AnyObject]()
        dict["characterId"] = characterId
        dict["membershipType"] = membershipType.rawValue
        dict["itemId"] = itemId
        return dict
    }
}