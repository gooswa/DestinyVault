//
//  DestinyPlatform.swift
//  DestinyVault
//
//  Created by Justin Gaussoin on 3/13/15.
//  Copyright (c) 2015 Justin Gaussoin. All rights reserved.
//

import Foundation

public enum DestinyPlatform:Int {
    case PSN = 2
    case XBOX = 1
    
    var login:String {
        switch self {
        case .PSN:
            return "Psnid"
        case .XBOX:
            return "Xbox"
        }
    }
    
    var platform:String {
        switch self {
        case .PSN:
            return "PSN"
        case .XBOX:
            return "Xbox"
        }
    }
}
