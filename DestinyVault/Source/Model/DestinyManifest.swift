//
//  DestinyManifest.swift
//  DestinyVault
//
//  Created by Justin Gaussoin on 3/11/15.
//  Copyright (c) 2015 Justin Gaussoin. All rights reserved.
//

import Foundation

public enum ColumnType : Int {
    case Integer = 1
    case Float = 2
    case Text = 3
    case Blob = 4
    case Null = 5
    
    static func fromSQLiteColumnType(columnType:Int32) -> ColumnType {
        switch columnType {
        case SQLITE_INTEGER:
            return .Integer
        case SQLITE_TEXT:
            return .Text
        case SQLITE_NULL:
            return .Null
        case SQLITE_FLOAT:
            return .Float
        case SQLITE_BLOB:
            return .Blob
        default:
            return .Text
        }
    }
}


class DestinyManifest {
    
    static let UpdateKey:String = "DestinyManifestUpdateDateKey"
    static let AppUpdateKey:String = "DestinyManifestAppUpdateKey"
    static let FileKey:String = "DestinyManifestFileKey"
    static let DatabaseFileKey:String = "DestinyManifestDatabaseFileKey"
    
    
    
    static let DestinyBucketTable:String = "DestinyInventoryBucketDefinition"
    static let DestinyBucketHash:String = "bucketHash"
    
    static let DestinyItemTable:String = "DestinyInventoryItemDefinition"
    static let DestinyItemHash:String = "itemHash"
    
    
    var _callback:(() -> Void)?
    var manifestPath:String!
    var filePath:NSURL!
    var databasePath:NSURL!
    private var db:COpaquePointer = nil
    
    private var _tables:Dictionary<String, [Int: [String:AnyObject]]> = Dictionary<String, [Int: [String:AnyObject]]>()
    
    init() {
        
    }
    
    
    func checkManifestUpdate(callback:() -> Void) {
        _callback = callback
        
        let defaults = NSUserDefaults.standardUserDefaults()
        let date = NSDate()
        
        /*
        if let lastUpdateDate:NSDate = defaults.objectForKey(DestinyManifest.AppUpdateKey) as? NSDate {
            let timeDifference = date.timeIntervalSince1970 - lastUpdateDate.timeIntervalSince1970
            if timeDifference < 43200 { //1800 = 30min //3600 == 1hr //21600 = 6hr //43200 = 12hr //86400 = 24hr
                
                self.loadTableFromArchive(DestinyManifest.DestinyBucketTable)
                self.loadTableFromArchive(DestinyManifest.DestinyItemTable)
                
                callback()
                return
            }
        }
        defaults.setObject(date, forKey: DestinyManifest.AppUpdateKey)
        */
        
        var client:DestinyClient = DestinyModel.sharedInstance.client
        var endpoint:DestinyAPI = DestinyAPI.Manifest
        client.apiRequest(endpoint) {[unowned self] (response, error) in
            if let rsp = response as? [String:AnyObject],
                let assetPath = rsp["mobileWorldContentPaths"] as? [String:AnyObject],
                    let langPath = assetPath["en"] as? String {
                self.manifestPath = "\(DestinyAPI.baseURL)\(langPath)"
                self.updateManifest()
            }
            
//            if let rsp = response as? [String:AnyObject],
//                let verions = rsp["mobileGearAssetDataBases"] as? [AnyObject],
//                    let lastVersion = verions[1] as? [String:AnyObject],
//                    let versionPath = lastVersion["path"] {
//     
//                    self.manifestPath = "\(DestinyAPI.baseURL)\(versionPath)"
//                    self.checkHeadersForUpdate()
//            }
            
            

//            if let rsp = response as? [String:AnyObject],
//                let assetPath = rsp["mobileAssetContentPath"] as? String {
//                    self.manifestPath = "\(DestinyAPI.baseURL)\(assetPath)"
//                    self.checkHeadersForUpdate()
//            }

        }
    }


    func checkHeadersForUpdate() {

        let defaults = NSUserDefaults.standardUserDefaults()
        var lastUpdateString:String? = defaults.stringForKey(DestinyManifest.UpdateKey)
        
        var client:DestinyClient = DestinyModel.sharedInstance.client
        client.headerRequest(self.manifestPath) {[unowned self] (headers) in
            
            if let lastUpdateHeader:String = headers["Last-Modified"] as? String
                where lastUpdateString != lastUpdateHeader {
                    defaults.setObject(lastUpdateHeader, forKey: DestinyManifest.UpdateKey)
                    self.updateManifest()
            } else {
                
                self.loadTableFromArchive(DestinyManifest.DestinyBucketTable)
                self.loadTableFromArchive(DestinyManifest.DestinyItemTable)

                self._callback?()

            }
        }

    }
    
    func updateManifest() {
 
        
        let defaults = NSUserDefaults.standardUserDefaults()
        let fileManager:NSFileManager = NSFileManager.defaultManager()
        
        var client:DestinyClient = DestinyModel.sharedInstance.client
        
        client.downloadManifest(self.manifestPath) {(filePath:NSURL) in
            defaults.setObject(filePath.absoluteString, forKey: DestinyManifest.FileKey)
            self.filePath = NSURL(string: filePath.absoluteString!)

            var destinationPath = filePath.URLByDeletingLastPathComponent
            destinationPath = destinationPath!.URLByAppendingPathComponent("database")
            let path = destinationPath!.path
            
            var error:NSError?
            let zipped = SSZipArchive.unzipFileAtPath(self.filePath.path, toDestination: path, overwrite: true, password: "", error: &error)
            
            if !zipped {
                println(error)
                return
            }
            
            let databaseFileURL = destinationPath!.URLByAppendingPathComponent(self.filePath.lastPathComponent!)
            self.databasePath = databaseFileURL
            if fileManager.fileExistsAtPath(databaseFileURL.path!) {
                println("Exists \(databaseFileURL.path!)")
                self.populateDictionary()
            }

            
        }
    }
    
    func populateDictionary() {
        let fm = NSFileManager.defaultManager()
        let dbPath = self.databasePath.path!
        
        let cpath = dbPath.cStringUsingEncoding(NSUTF8StringEncoding)
        let error = sqlite3_open_v2(cpath!, &db, SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE, nil)
        if error != SQLITE_OK {
            println("SQLiteDB - failed to open DB!")
            sqlite3_close(db)
        }
        
        
       
        //Prepare Buckets
        println("Prepare Buckets")
        let tablesSQL = "select name from sqlite_master where type='table'"
        let tablesStatement = prepareStatement(tablesSQL)
        let tables = executeQuery(tablesStatement)
        println(tables)
        
        
        return
        populateFromTable(DestinyManifest.DestinyBucketTable, hashName: DestinyManifest.DestinyBucketHash)
        populateFromTable(DestinyManifest.DestinyItemTable, hashName: DestinyManifest.DestinyItemHash)
        
        
        sqlite3_close(db)

        self._callback?()
    }
    
    
    func findDefinition(tableName:String, hash:Int) -> [String:AnyObject]? {
        if let table:Dictionary<Int, [String:AnyObject]> = _tables[tableName] {
            return table[hash]
        }
        return nil
    }
    
    
    func loadTableFromArchive(tableName:String) {
        let fm = NSFileManager.defaultManager()
        let url:NSURL = createFilePath("\(tableName).hash")
        
        
        if fm.fileExistsAtPath(url.path!) {

            let dict = NSKeyedUnarchiver.unarchiveObjectWithFile(url.path!) as? Dictionary<Int, [String:AnyObject]>
            _tables[tableName] = dict
        }
    }
    
    func populateFromTable(tableName:String, hashName:String) {
        var bucketSql = "select id, json from \(tableName)"
        let bucketStatement = prepareStatement(bucketSql)
        let bucketRows = executeQuery(bucketStatement)
        
        var tempDict = Dictionary<Int, [String:AnyObject]>()
        
        for row in bucketRows {
            let jsonString:String = row["json"] as! String
            let jsonData:NSData = jsonString.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)!
            var error:NSError?
            let json:[String:AnyObject] = NSJSONSerialization.JSONObjectWithData(jsonData, options: NSJSONReadingOptions(0), error: &error) as! [String:AnyObject]
            
            if error == nil {
                if let intHash:Int = json[hashName]!.integerValue {
                    tempDict[intHash] = json
                }
                
            }
        }
        _tables[tableName] = tempDict

        let url:NSURL = createFilePath("\(tableName).hash")
        var success = NSKeyedArchiver.archiveRootObject(tempDict, toFile: url.path!)
        if success {
            println("Completed Archive")
        } else {
            println("Error writing archive \(url.path!)")
        }
    }
    
    func createFilePath(fileName:String) -> NSURL {
        let fm = NSFileManager.defaultManager()
        let dirURL = fm.URLForDirectory(.DocumentDirectory, inDomain: .UserDomainMask, appropriateForURL: nil, create: false, error: nil)
        return dirURL!.URLByAppendingPathComponent(fileName)
    }
    
    
    
    private func prepareStatement(sql:String) -> COpaquePointer {
        var stmt:COpaquePointer = nil
        var cSql = sql.cStringUsingEncoding(NSUTF8StringEncoding)
        // Prepare
        let result = sqlite3_prepare_v2(self.db, cSql!, -1, &stmt, nil)
        if result != SQLITE_OK {
            sqlite3_finalize(stmt)
            if let error = String.fromCString(sqlite3_errmsg(self.db)) {
                let msg = "SQLiteDB - failed to prepare SQL: \(sql), Error: \(error)"
                println(msg)
            }
            return nil
        }
        
        return stmt
    }
    
    
    func executeQuery(stmt:COpaquePointer) -> [Dictionary<String,AnyObject>] {
        var columnTypes = [ColumnType]()
        var columnNames = [String]()
        var columnCount:CInt = 0
        
        
        columnCount = sqlite3_column_count(stmt)
        for index in 0..<columnCount {
            
            // Get column name
            let name = sqlite3_column_name(stmt, index)
            columnNames.append(String.fromCString(name)!)
            let columnType = sqlite3_column_type(stmt, Int32(index))
            
            // Get column type
            var cType = ColumnType.fromSQLiteColumnType(columnType)
            columnTypes.append(cType)
        }
        
        var rows = [Dictionary<String,AnyObject>]()
        var result = sqlite3_step(stmt)

        while result == SQLITE_ROW {
            var row = [String:AnyObject]()
            
            for index in 0..<columnCount {
                let key = columnNames[Int(index)]
                let type = columnTypes[Int(index)]
                
                let value:AnyObject
                switch type {
                case .Integer:
                    let buf = sqlite3_column_int64(stmt, index)
                    value = 0 //Ignored because of returned values over 32bit return incorrect
                default:
                    let buf = UnsafePointer<Int8>(sqlite3_column_text(stmt, index))
                    value = String.fromCString(buf)!
                }
                
                row[key] = value
                //println(row)
            }
            rows.append(row)
            
            result = sqlite3_step(stmt)
        }
        
        sqlite3_finalize(stmt)
        return rows
    }
}