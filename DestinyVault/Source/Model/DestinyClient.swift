//
//  DestinyClient.swift
//  DestinyVault
//
//  Created by Justin Gaussoin on 3/13/15.
//  Copyright (c) 2015 Justin Gaussoin. All rights reserved.
//

import Foundation
import Alamofire



class DestinyClient {
    
    private let _apiKey:String
    
    private var callback:((DestinyUser?, NSError?) -> Void)?
    
    var _alamofire: Alamofire.Manager!
    var loggedIn:Bool = false
    
    let platform:DestinyPlatform
    let cookies = NSHTTPCookieStorage.sharedHTTPCookieStorage()
    
    
    var validLogin:Bool {
        get {
            var validCount:UInt = 0
            let bungieCookies = cookies.cookiesForURL( NSURL(string: DestinyAPI.baseURL)! ) as! [NSHTTPCookie]
            let cookieCount = bungieCookies.filter { contains(DestinyAPI.cookieNames, $0.name) }.count
            
            if cookieCount == DestinyAPI.cookieNames.count {
                return true
            }
            
            return false
        }
    }

    
    init(apiKey:String, platform pform:DestinyPlatform = DestinyPlatform.PSN) {
        _apiKey = apiKey
        platform = pform
        let cfg = NSURLSessionConfiguration.defaultSessionConfiguration()
        cfg.HTTPCookieStorage = cookies
        _alamofire = Alamofire.Manager(configuration: cfg)
    }
    
    
    private func completeLogin() {
        let bungieCookies = cookies.cookiesForURL( NSURL(string: DestinyAPI.baseURL)! ) as! [NSHTTPCookie]
        DestinyAPI.cookies = NSHTTPCookie.requestHeaderFieldsWithCookies(bungieCookies)
        DestinyAPI.key = self._apiKey
        DestinyAPI.csrf = bungieCookies.filter { $0.name == DestinyAPI.bungledCookie }[0].value
        self.loggedIn = true
    }
    
    
    private func getBrandingParams(data:String) -> String? {
        var brandingParams:String?
        var regexError: NSError?
        let pattern:String = "id=\"brandingParams\"[\\s\\w=\"]+value=\"([\\w=]+)\""
        let regex = NSRegularExpression(pattern: pattern, options: .CaseInsensitive, error: &regexError)!
        
        if let err = regexError {
            self.callback?(nil, err)
            return nil
        }
        
        let range = NSMakeRange(0, count(data));
        let matches = regex.matchesInString(data, options: nil, range: range) as! [NSTextCheckingResult]
        for match in matches {
            
            let range:NSRange = match.rangeAtIndex(1)
            let startIndex = advance(data.startIndex, range.location)
            let endIndex = advance(data.startIndex, (range.location+range.length))
            
            return data.substringWithRange(Range<String.Index>(start: startIndex, end: endIndex))
        }
        return nil
    }
    
    
    func authenticate(username:String, password:String, _ _callback:(DestinyUser?, NSError?) -> Void) {
        //println("Authenticate with Bungie")
        callback = _callback
        
        var endpoint:DestinyAPI = DestinyAPI.Login(platform)
        _alamofire.request(endpoint)
            .responseString({ (request, response, data, error) in
                
                if let err = error {
                    _callback(nil, err)
                    return
                }
                
                if self.validLogin {
                    self.completeLogin()
                    self.getCurrentUser()
                    return
                }
                
                //println("Need Platform Authentication")
               
                if let bp = self.getBrandingParams(data!) {
                    self.authenticatePSN(username, password, bp)
                }
            })
    }
    
    
    func authenticatePSN(username:String, _ password:String, _ brandingParams:String) {
        
        let params = ["j_username": username, "j_password": password, "params": brandingParams]
        _alamofire.request(.POST, "https://auth.api.sonyentertainmentnetwork.com/login.do", parameters: params, encoding: .URL)
        .responseString({(request, response, string, error) in
            if self.validLogin {
                self.completeLogin()
                self.getCurrentUser()
                return
            } else {
                var userInfo = [
                    NSLocalizedDescriptionKey : "Username or Password was invalid."
                ]
                var error:NSError = NSError(domain: "DestinyClientErrorDomain", code: 2, userInfo: userInfo)
                self.callback?(nil, error)
            }
        })
        
    }
    
    
    func getCurrentUser() {
        
        //println("getCurrentUser")
        
        var endpoint:DestinyAPI = DestinyAPI.CurrentUser
        
        apiRequest(endpoint) {(response, error) in
            
            if let rsp = response as? [String:AnyObject],
                let user = rsp["user"] as? [String:AnyObject] {
                    
                let gt:String? = rsp["psnId"] as? String
   
                var ep:DestinyAPI = DestinyAPI.MembershipIds
                    
                self.apiRequest(ep) {(response, error) in
                    if let rsp = response as? [String:AnyObject] {
                        for (key, value) in rsp {
                            if let val = value as? Int where val == DestinyPlatform.PSN.rawValue {
                                
                                self.loggedIn = true
                                
                                let duser = DestinyUser(
                                    tag: gt!,
                                    type: DestinyPlatform.PSN,
                                    id: key as String)
                                
                                self.callback?(duser, nil)
                                break
                            }
                        }
                    }
                    
                }
            }
            
        }
    }
    
    func getVault(user:DestinyUser, _ callback:(([AnyObject]) -> Void)) {
        //println("getVault")
        var endpoint:DestinyAPI = DestinyAPI.Vault(user)
        
        //println("endpoint resolved")
        apiRequest(endpoint) {[unowned self] (response, error) in
            //          println("vault responded")

            if let rsp = response as? [String:AnyObject],
                let data = rsp["data"] as? [String:AnyObject],
                let buckets = data["buckets"] as? [AnyObject] {
                    
                    callback(buckets)
                    
            }
        }
        
        
    }

    func getCharacterInventory(user:DestinyUser, character:DestinyCharacter, _ callback:(([String:AnyObject]) -> Void)) {
        //println("getCharacterInventory")
        var endpoint:DestinyAPI = DestinyAPI.CharacterInventory(user, character)
        apiRequest(endpoint) {[unowned self] (response, error) in
            
            if let rsp = response as? [String:AnyObject],
                let data = rsp["data"] as? [String:AnyObject],
                let buckets = data["buckets"] as? [String:AnyObject] {
                    callback(buckets)
                    
            }
        }
    }
    
    func getAccount(user:DestinyUser, _ callback:((AnyObject?) -> Void)) {
        
        var endpoint:DestinyAPI = DestinyAPI.Account(user)
        apiRequest(endpoint) {[unowned self] (response, error) in
            
            if let rsp = response as? [String:AnyObject] {
                let data: AnyObject? = rsp["data"] as AnyObject?
                callback(data)
            }
        }
        
    }
    
    
    func headerRequest(url:String, callback:([NSObject:AnyObject]) -> Void) {
        //println("headerRequest")
        
        let theURL:NSURL = NSURL(string:url)!
        let request:NSMutableURLRequest = NSMutableURLRequest(URL:theURL)
        let queue:NSOperationQueue = NSOperationQueue()
        
        NSURLConnection.sendAsynchronousRequest(request, queue: queue, completionHandler:{ response, data, error in
            if error != nil {
                println(error)
                return
            }
            if let rsp = response as? NSHTTPURLResponse {
                callback(rsp.allHeaderFields)
            }
            
        })
    }
    
    func downloadManifest(manifestPath:String, _ destination:(NSURL) -> Void) {
        
        var url:NSURL?
        Alamofire.download(.GET, manifestPath, {(temporaryURL, response) in
            
            if let directoryURL = NSFileManager.defaultManager()
                .URLsForDirectory(.DocumentDirectory,
                    inDomains: .UserDomainMask)[0]
                as? NSURL {
                    let pathComponent = response.suggestedFilename
                    url = directoryURL.URLByAppendingPathComponent(pathComponent!)
            } else {
                url = temporaryURL
            }
            return url!
            
            
        }).response({(request, response, _, error) in
            
            if let u = url {
                destination(u)
            }
            
        })
    }
    
    
    func apiRequest(endpoint:DestinyAPI, _ callback:((AnyObject?, NSError?) -> Void)) {
        
        _alamofire.request(endpoint).responseJSON
            {(request, response, data, error) in
                
                if let err = error {
                    callback(nil, error)
                    return
                }
                
                let json = data as! [String:AnyObject]
                
                if let errorCode:Int = json["ErrorCode"] as? Int where errorCode != 1 {
                    
                    let errorMessage:String = json["Message"] as! String
                    var userInfo = [ NSLocalizedDescriptionKey : errorMessage ]
                    var apiError = NSError(domain: "DestinyDomain", code: 10, userInfo: userInfo)
                    callback(nil, error)
                    return
                }
                
                if let throttleSeconds:Int = json["ThorttleSeconds"] as? Int where throttleSeconds > 0 {
                    let throttleFloat = Float(throttleSeconds)
                    
                    DelayClosure(throttleFloat, {
                        let rsp: AnyObject? = json["Response"] as AnyObject?
                        callback(rsp, nil)
                    })
                    
                } else {
                    let rsp: AnyObject? = json["Response"] as AnyObject?
                    callback(rsp, nil)
                }
                
                
        }
        
    }
    
}


