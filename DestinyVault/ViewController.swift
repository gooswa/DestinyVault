//
//  ViewController.swift
//  DestinyVault
//
//  Created by Justin Gaussoin on 3/13/15.
//  Copyright (c) 2015 Justin Gaussoin. All rights reserved.
//

import UIKit

public enum MainMenuMode {
    case Selection
    case Transfer
}

class ViewController: UIViewController {

    var _model:DestinyModel!
    var mainView:MainView { return self.view as! MainView }
    var activeMenuItem:MenuItemView?
    var activeViewController:UIViewController?
    
    var transferOverlayView:UIView = UIView(frame: MainView.contentFrame)
    var overlayTapGesture:UITapGestureRecognizer?
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    
    var menuMode:MainMenuMode = .Selection {
        didSet {
            
            if menuMode == .Selection {
                
                for menuItem in mainView.menuItems {
                    menuItem.removeTarget(self, action: Selector("transferItemSelection:"), forControlEvents: .TouchUpInside)
                    menuItem.addTarget(self, action: Selector("menuItemSelection:"), forControlEvents: .TouchUpInside)
                }
                
            } else {
                
                for menuItem in mainView.menuItems {
                    menuItem.removeTarget(self, action: Selector("menuItemSelection:"), forControlEvents: .TouchUpInside)
                    menuItem.addTarget(self, action: Selector("transferItemSelection:"), forControlEvents: .TouchUpInside)
                }
                
            }
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        _model = DestinyModel.sharedInstance
        mainView.sliderView.hidden = true

        transferOverlayView.backgroundColor = UIColor(hex: 0, 0.6)
        overlayTapGesture = UITapGestureRecognizer(target: self, action: Selector("closeTransferItemSelection:"))
        transferOverlayView.addGestureRecognizer(overlayTapGesture!)
        
        var lv = LoadingView()
        lv.play()
        lv.frame.origin = CGPointMake( view.bounds.width/2 - lv.frame.width/2 ,  view.bounds.height/2 - lv.frame.height/2 )
        view.addSubview(lv)
        
        
        _model.login {user, error in
            if error != nil {
                println(error)
                return
            }
            lv.removeFromSuperview()
            self.buildSliderMenu()
        }
        let noteCenter = NSNotificationCenter.defaultCenter()
        noteCenter.addObserver(self, selector: Selector("openTransferItemTarget:"), name: "TranferItemTargetOpen", object: nil)
    }
    
    
    func buildSliderMenu() {
        mainView.sliderView.hidden = false
 
        var vaultHeader:MenuItemView = MenuItemView()
        vaultHeader.menuType = .Vault
        vaultHeader.backgroundColor = UIColor(hex: 0x3a3a3a)
        vaultHeader.emblemImageView.image = UIImage(named: "VaultEmblem")
        vaultHeader.classLabel.text = "Tower Vault"
        mainView.sliderView.addSubview(vaultHeader)
        mainView.menuItems.append(vaultHeader)
     
        activeMenuItem = vaultHeader
        
        let characters = _model.characters
        var i:Int = 1
        for character in characters {
            var headerView = MenuItemView()
            headerView.menuType = .Character
            headerView.characterIndex = i-1
            headerView.characterId = character.0
            let burl:NSURL = NSURL(string: "\(DestinyAPI.baseURL)\(character.1.backgroundURL)")!
            headerView.backgroundImageView.hnk_setImageFromURL(burl)
            
            let eurl:NSURL = NSURL(string: "\(DestinyAPI.baseURL)\(character.1.emblemURL)")!
            headerView.emblemImageView.hnk_setImageFromURL(eurl)
            
            
            headerView.classLabel.text = character.1.className
            headerView.levelLabel.text = String(character.1.level)
            headerView.frame.origin.y += CGFloat(i) * headerView.frame.height
            mainView.sliderView.addSubview(headerView)
            mainView.menuItems.append(headerView)
            i++
        }
        menuMode = .Selection
        self.initVault()
    }
    
    
    func initVault() {
        
        var lv:LoadingView = LoadingView(frame: MainView.contentFrame)
        lv.play()
        mainView.addSubview(lv)
        
        _model.vault.update {
            lv.removeFromSuperview()
            
            var vaultController:VaultViewController = VaultViewController()
            self.mainView.contentView.addSubview(vaultController.vaultView)
            vaultController.vault = self._model.vault
            self.activeViewController = vaultController
        }
    }
    
    func openTransferItemTarget(note:NSNotification) {
        menuMode = .Transfer
        
        mainView.insertSubview(transferOverlayView, belowSubview: mainView.sliderView)
        
        transferOverlayView.alpha = 0
        self.activeMenuItem?.userInteractionEnabled = false
        UIView.animateWithDuration(0.3, animations: {
            self.activeMenuItem?.alpha = 0.0
            self.transferOverlayView.alpha = 1.0
            self.mainView.sliderView.frame.origin.y = MainView.openTransferPosition
            self.mainView.slideItemsUp()
        }, completion: {finished in
                
        })
    }
    
    
    func transferItemSelection(item:MenuItemView) {
        let noteCenter = NSNotificationCenter.defaultCenter()
        let notification:NSNotification = NSNotification(name: "TranferItemTargetSelected", object: item.characterId)
        noteCenter.postNotification(notification)
       
        self.closeTransferItemSelection(nil)
    }
    
    
    func closeTransferItemSelection(event:AnyObject?) {
        menuMode = .Selection
        self.activeMenuItem?.userInteractionEnabled = true
        transferOverlayView.alpha = 1.0
        UIView.animateWithDuration(0.3, animations: {
            self.transferOverlayView.alpha = 0
            self.activeMenuItem?.alpha = 1
            self.mainView.sliderView.frame.origin.y = MainView.closedPosition
            self.mainView.slideItemsDown()
            }, completion: {finished in
                self.transferOverlayView.removeFromSuperview()
        })
    }
    
    func menuItemSelection(item:MenuItemView) {
        
        if item === activeMenuItem {
            return
        }
        
        if let am = activeMenuItem {
            println(item)
            
            var moveY = item.frame.origin.y
            activeMenuItem?.setY(y: moveY)
            item.setY(y: 0)
            
        }
        activeMenuItem = item
        mainView.closeMenu()
        
        
        if let ac = activeViewController {
            ac.view.removeFromSuperview()
            activeViewController = nil
        }
        
        
        var lv:LoadingView = LoadingView(frame: MainView.contentFrame)
        lv.play()
        mainView.insertSubview(lv, belowSubview: mainView.sliderView)
        
        if item.menuType == .Character {
            
            if !item.characterId.isEmpty {
                let character = _model.characters[item.characterId]
                
                
                
                character?.update {
                    lv.removeFromSuperview()
                    
                    var characterController:CharacterEquipViewController = CharacterEquipViewController()
                    self.mainView.contentView.addSubview(characterController.equipmentView)
                    
                    characterController.character = character

                    self.activeViewController = characterController
                }
                
            }
            
        } else if item.menuType == .Vault {
            let vault = _model.vault

            vault.update {
                lv.removeFromSuperview()
                
                var vaultController:VaultViewController = VaultViewController()
                self.mainView.contentView.addSubview(vaultController.vaultView)
                
                vaultController.vault = vault
                
                self.activeViewController = vaultController
            }
        }
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

